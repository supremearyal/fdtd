// To compile:
//
// Windows (MinGW): g++ -std=c++11 fdtd.cpp -mwindows -lmingw32
//                  -lSDL2main -lSDL2 -O3
// Linux: g++ -std=c++11 fdtd.cpp `sdl2-config --cflags --libs` -O3

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <memory>
#include <SDL.h>

const double pi = atan(1) * 4;

struct FDTD
{
    // -----------
    // User input.
    // -----------

    // Permittivity.
    double epsilon;

    // Permeability.
    double mu;

    // CFL limit.
    double CFLN;

    // Simulation domain.
    double Dx;
    double Dy;
    double Dz;

    // Grid size.
    int Nx;
    int Ny;
    int Nz;

    // Source signature constants.
    double tw;
    double to;

    // Current source.
    std::function <double (double)> J;

    // Source location.
    int sourceNodeI;
    int sourceNodeJ;
    int sourceNodeK;

    // Observation point.
    int outNodeI;
    int outNodeJ;
    int outNodeK;

    // Simulation time.
    double simTime;

    // -----------
    // Initialize.
    // -----------

    // Speed of propagation.
    double cMax;

    // Number of nodes.
    int nx;
    int ny;
    int nz;

    // Space step.
    double dx;
    double dy;
    double dz;

    // Time step.
    double dt;

    // Number of timesteps.
    int nMax;

    // E and H field arrays.
    std::unique_ptr<double[]> Ex;
    std::unique_ptr<double[]> Ey;
    std::unique_ptr<double[]> Ez;

    std::unique_ptr<double[]> Hx;
    std::unique_ptr<double[]> Hy;
    std::unique_ptr<double[]> Hz;
    
    // Output array.
    std::unique_ptr<double[]> Eout;
    
    // Constants.
    double cExy;
    double cExz;
    double cEyx;
    double cEyz;
    double cEzx;
    double cEzy;

    double cHxy;
    double cHxz;
    double cHyx;
    double cHyz;
    double cHzx;
    double cHzy;

    double dte;

    // Simulation time step count.
    int n;
    
    FDTD() :
        epsilon(8.85418782e-12),
        mu(4 * pi * 1e-7),
        CFLN(0.99),

        Dx(1.0 / 250),
        Dy(1.0),
        Dz(1.0),

        Nx(2),
        Ny(500),
        Nz(500),

        tw(5e-10),
        to(4 * tw),

        J([&] (double t) -> double
          {
              double tDiff = t - to;
              // Differentiated Gaussian pulse.
              return -tDiff / tw * exp(-(tDiff * tDiff) / (tw * tw));
          }),

        sourceNodeI(Nx / 2),
        sourceNodeJ(Ny / 2),
        sourceNodeK(Nz / 2),

        outNodeI(sourceNodeI),
        outNodeJ(sourceNodeJ),
        outNodeK(sourceNodeK),

        simTime(4e-7),

        cMax(1 / sqrt(mu * epsilon)),

        nx(Nx + 1),
        ny(Ny + 1),
        nz(Nz + 1),

        dx(Dx / Nx),
        dy(Dy / Ny),
        dz(Dz / Nz),

        dt(CFLN /
           (cMax * sqrt(1 / (dx * dx) + 1 / (dy * dy) + 1 / (dz * dz)))),

        nMax(ceil(simTime / dt)),

        Ex(std::unique_ptr<double[]>(new double[nx * ny * nz])),
        Ey(std::unique_ptr<double[]>(new double[nx * ny * nz])),
        Ez(std::unique_ptr<double[]>(new double[nx * ny * nz])),

        Hx(std::unique_ptr<double[]>(new double[nx * ny * nz])),
        Hy(std::unique_ptr<double[]>(new double[nx * ny * nz])),
        Hz(std::unique_ptr<double[]>(new double[nx * ny * nz])),

        Eout(std::unique_ptr<double[]>(new double[nMax])),

        cExy(dt / (epsilon * dy)),
        cExz(dt / (epsilon * dz)),
        cEyx(dt / (epsilon * dx)),
        cEyz(dt / (epsilon * dz)),
        cEzx(dt / (epsilon * dx)),
        cEzy(dt / (epsilon * dy)),

        cHxy(dt / (mu * dy)),
        cHxz(dt / (mu * dz)),
        cHyx(dt / (mu * dx)),
        cHyz(dt / (mu * dz)),
        cHzx(dt / (mu * dx)),
        cHzy(dt / (mu * dy)),

        dte(dt / epsilon),

        n(0)
    {
        // Initialize fields to zero.
        for(int i = 0; i < nx * ny * nz; ++i)
            Ex[i] = Ey[i] = Ez[i] = Hx[i] = Hy[i] = Hz[i] = 0;
    }
    
    // Indexing macro.
#define _(a, b, c) ((a) * ny * nz + (b) * nz + (c))
    void updateElectricField()
    {
        // E-field update equations.

        // Update Ex.
        for(int i = 0; i < nx - 1; ++i)
            for(int j = 1; j < ny - 1; ++j)
                for(int k = 1; k < nz - 1; ++k)
                    Ex[_(i, j, k)] += cExy * (Hz[_(i, j, k)] -
                                              Hz[_(i, j - 1, k)]) -
                        cExz * (Hy[_(i, j, k)] -
                                Hy[_(i, j, k - 1)]);

        // Update Ey.
        for(int i = 1; i < nx - 1; ++i)
            for(int j = 0; j < ny - 1; ++j)
                for(int k = 1; k < nz - 1; ++k)
                    Ey[_(i, j, k)] += cEyz * (Hx[_(i, j, k)] -
                                              Hx[_(i, j, k - 1)]) -
                        cEyx * (Hz[_(i, j, k)] -
                                Hz[_(i - 1, j, k)]);

        // Update Ez.
        for(int i = 1; i < nx - 1; ++i)
            for(int j = 1; j < ny - 1; ++j)
                for(int k = 0; k < nz - 1; ++k)
                    Ez[_(i, j, k)] += cEzx * (Hy[_(i, j, k)] -
                                              Hy[_(i - 1, j, k)]) -
                        cEzy * (Hx[_(i, j, k)] -
                                Hx[_(i, j - 1, k)]);
    }

    void updateMagneticField()
    {
        // H-field update equations.

        // Update Hx.
        for(int i = 0; i < nx; ++i)
            for(int j = 0; j < ny - 1; ++j)
                for(int k = 0; k < nz - 1; ++k)
                    Hx[_(i, j, k)] -= cHxy * (Ez[_(i, j + 1, k)] -
                                              Ez[_(i, j, k)]) -
                        cHxz * (Ey[_(i, j, k + 1)] -
                                Ey[_(i, j, k)]);

        // Update Hy.
        for(int i = 0; i < nx - 1; ++i)
            for(int j = 0; j < ny; ++j)
                for(int k = 0; k < nz - 1; ++k)
                    Hy[_(i, j, k)] -= cHyz * (Ex[_(i, j, k + 1)] -
                                              Ex[_(i, j, k)]) -
                        cHyx * (Ez[_(i + 1, j, k)] -
                                Ez[_(i, j, k)]);


        // Update Hz.
        for(int i = 0; i < nx - 1; ++i)
            for(int j = 0; j < ny - 1; ++j)
                for(int k = 0; k < nz; ++k)
                    Hz[_(i, j, k)] -= cHzx * (Ey[_(i + 1, j, k)] -
                                              Ey[_(i, j, k)]) -
                        cHzy * (Ex[_(i, j + 1, k)] -
                                Ex[_(i, j, k)]);
    }

    void exciteCurrentSource()
    {
        // E-source equation.
        Ex[_(sourceNodeI, sourceNodeJ, sourceNodeK)] -=
            J((n + 0.5) * dt) * dte;
    }

    void outputData()
    {
        Eout[n] = Ez[_(outNodeI, outNodeJ, outNodeK)];
    }

    bool step()
    {
        if(n < nMax)
        {
            updateElectricField();
            exciteCurrentSource();
            updateMagneticField();
            outputData();
            
            n++;

            return true;
        }
        else
        {
            return false;
        }
    }
};

struct Window
{
    // Window size.
    int width;
    int height;
    
    // Frame rate.
    const int FPS;
    const Uint32 FPS_TIME;

    // Color palette.
    static const int NUM_COLORS = 9;
    const std::array<std::array<int, 3>, NUM_COLORS> colors;

    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Texture *texture;

    FDTD simulation;
    
    Window() :
        FPS(25),
        FPS_TIME(1000.0 / FPS),

        // MATLAB Jet color palette.
        colors({{{0x00, 0x00, 0x90},
                {0x00, 0x0f, 0xff},
                {0x00, 0x90, 0xff},
                {0x0f, 0xff, 0xee},
                {0x90, 0xff, 0x70},
                {0xff, 0xee, 0x00},
                {0xff, 0x70, 0x00},
                {0xee, 0x00, 0x00},
                {0x7f, 0x00, 0x00}}})
    {
        width = simulation.ny;
        height = simulation.nz;
        
        // Initialize window.
        window = SDL_CreateWindow("3D-FDTD : Cavity Resonator",
                                  SDL_WINDOWPOS_UNDEFINED,
                                  SDL_WINDOWPOS_UNDEFINED,
                                  width,
                                  height,
                                  SDL_WINDOW_SHOWN);
        if(!window)
        {
            fprintf(stderr, "%s: %s\n", "Couldn't create SDL window",
                    SDL_GetError());
            exit(1);
                
        }

        // Initialize renderer. 0 means hardware acceleration gets
        // priority.
        renderer = SDL_CreateRenderer(window, -1, 0);
        if(!renderer)
        {
            fprintf(stderr, "%s: %s\n", "Couldn't create SDL renderer",
                    SDL_GetError());
            exit(1);
        }

        // Initialize texture.
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888,
                                    SDL_TEXTUREACCESS_STREAMING, width,
                                    height);
        if(!texture)
        {
            fprintf(stderr, "%s: %s\n", "Couldn't create SDL texture",
                    SDL_GetError());
            exit(1);
        }
    }

    bool userQuit()
    {
        // Check to see if user wants to quit.
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            if(event.type == SDL_QUIT ||
               (event.type == SDL_KEYDOWN &&
                event.key.keysym.sym == SDLK_ESCAPE))
                return true;
        }

        return false;
    }

    // Interpolate color between a and b into c by ratio.
    static void colorInterpolate(int ra, int ga, int ba,
                          int rb, int gb, int bb,
                          int &rc, int &gc, int &bc,
                          double ratio)
    {
        rc = floor((rb - ra) * ratio + ra);
        gc = floor((gb - ga) * ratio + ga);
        bc = floor((bb - ba) * ratio + ba);
    }

    // Get color for value where minValue <= value <= maxValue
    Uint32 getColor(double value, double minValue, double maxValue)
    {
        int colorIndexA = 0;
        int colorIndexB = 0;
        double colorRatio = 0;
        if(maxValue - minValue != 0)
        {
            double colorScale = (value - minValue) / (maxValue - minValue);
            double colorIndex = colorScale * (NUM_COLORS - 1);
            colorIndexA = floor(colorIndex);
            colorIndexB = ceil(colorIndex);
            colorRatio = colorIndex - colorIndexA;
        }

        // Find interpolated color.
        int colorR = 0;
        int colorG = 0;
        int colorB = 0;
        colorInterpolate(colors[colorIndexA][0],
                         colors[colorIndexA][1],
                         colors[colorIndexA][2],
                         colors[colorIndexB][0],
                         colors[colorIndexB][1],
                         colors[colorIndexB][2],
                         colorR, colorG, colorB,
                         colorRatio);

        // Get color value from surface.
        return (0xff << 24) | (colorR << 16) | (colorG << 8) | (colorB);
    }

    void drawField()
    {
        // Draw Ez field when x = outNodeI (yz plane).
        int nx = simulation.nx;
        int ny = simulation.ny;
        int nz = simulation.nz;
        int outNodeI = simulation.outNodeI;

        // Get max, min value so we can colorize appropriately.
        double maxEx = -1e100;
        double minEx = 1e100;
        for(int j = 0; j < ny; ++j)
        {
            for(int k = 0; k < nz; ++k)
            {
                double currEx = simulation.Ex[_(outNodeI, j, k)];

                if(currEx > maxEx)
                    maxEx = currEx;

                if(currEx < minEx)
                    minEx = currEx;
            }
        }

        // Lock texture before drawing.
        void *pixels;
        int pitch;
        if(SDL_LockTexture(texture, nullptr, &pixels, &pitch) < 0)
        {
            fprintf(stderr, "Couldn't lock texture: %s\n", SDL_GetError());
            exit(1);
        }

        for(int j = 0; j < ny; ++j)
        {
            Uint32 *horizPixels = (Uint32*)((Uint8*) pixels + j * pitch);
            for(int k = 0; k < nz; ++k)
            {
                double currEx = simulation.Ex[_(outNodeI, j, k)];
                Uint32 color = getColor(currEx, minEx, maxEx);
                *horizPixels++ = color;
            }
        }

        SDL_UnlockTexture(texture);
    }

    void updateDisplay()
    {
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, texture, nullptr, nullptr);
        SDL_RenderPresent(renderer);
    }

    void loop()
    {
        // Keep track of how long update equations take.
        Uint32 startTime = SDL_GetTicks();
        while(simulation.step() && !userQuit())
        {
            drawField();
            updateDisplay();

            // Wait to give proper FPS.
            Sint32 delayTime = FPS_TIME - (SDL_GetTicks() - startTime);
            if(delayTime > 0)
                SDL_Delay(delayTime);

            startTime = SDL_GetTicks();
        }
    }

    void saveFrame()
    {
    }
    
    ~Window()
    {
        // Clean up.

        // Free SDL stuff.
        SDL_DestroyTexture(texture);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
    }
};

int main(int argc, char *argv[])
{
    // Initialize SDL stuff for display.
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        fprintf(stderr, "%s: %s\n", "Couldn't initialize SDL",
                SDL_GetError());
        return 1;
    }

    Window window;
    window.loop();

    // Close SDL.
    SDL_Quit();

    return 0;
}
